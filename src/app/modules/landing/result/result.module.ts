import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { ResultComponent } from 'app/modules/landing/result/result.component';
import { resultRoutes } from 'app/modules/landing/result/result.routing';

@NgModule({
    declarations: [
        ResultComponent
    ],
    imports     : [
        RouterModule.forChild(resultRoutes),
        SharedModule
    ]
})
export class ResultModule
{
    
}
