import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { FuseNavigationModule } from '@fuse/components/navigation/navigation.module';
import { EvaluateSidebarComponent } from './evaluate-sidebar.component';

@NgModule({
    declarations: [
        EvaluateSidebarComponent
    ],
    imports     : [
        RouterModule.forChild([]),
        MatIconModule,
        MatProgressBarModule,
        FuseNavigationModule
    ],
    exports     : [
        EvaluateSidebarComponent
    ]
})
export class EvaluateSidebarModule
{
}
