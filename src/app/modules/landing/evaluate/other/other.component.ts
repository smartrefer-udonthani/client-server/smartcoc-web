import { Component, ViewEncapsulation } from '@angular/core';
import { EvaluateService } from 'app/services/evaluate.service';

@Component({
    selector: 'landing-evaluate-other',
    templateUrl: './other.component.html',
    encapsulation: ViewEncapsulation.None
})
export class OtherComponent {
    sdate: any;
    edate: any;
    validateForm:boolean = false;
    id: any = {
        question_type_id: 22
    };
    /**
     * Constructor
     */
    constructor() {
    }
}
