import { Component } from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';
import { RegisterService } from '../../../services/register.service';

@Component({
    selector: 'landing-home',
    templateUrl: './evaluate.component.html'
})
export class EvaluateComponent {
    sdate: any;
    edate: any;
    validateForm: boolean = false;
    profilePatient: any;
    currentRoute = "";
    itemCatagory: any = [];
    catagory_name: string ='';

    /**
     * Constructor
     */
    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private registerService: RegisterService,
    ) {
        let username = sessionStorage.getItem('username');
        if(!username){
            this.router.navigate(['/sign-in']);
        }
        this.profilePatient = JSON.parse(localStorage.getItem('profileData'));
        // console.log(this.profilePatient);
       
        
        // this.activatedRoute.queryParams.subscribe(
        //     params => { this.profilePatient = JSON.parse(params['profile']); }
        // );

        // this.router.events.subscribe((event: Event) => {
        //     if (event instanceof NavigationStart) {
        //         // Show progress spinner or progress bar
        //         console.log('Route change detected');
        //     }

        //     if (event instanceof NavigationEnd) {
        //         // Hide progress spinner or progress bar
        //         this.currentRoute = event.url;
        //         console.log(event);
        //     }

        //     if (event instanceof NavigationError) {
        //         // Hide progress spinner or progress bar

        //         // Present error to user
        //         console.log(event.error);
        //     }
        // });

    }

    ngOnInit() {
        this.list_catagory();
      }

    async list_catagory() {
    let rs: any = await this.registerService.list_catagory();
    // console.log(rs);
    this.itemCatagory = rs;
    this.catagoryDescription(this.profilePatient.evaluate_cause);
    }

    async back() {
        let route = sessionStorage.getItem('route');
        this.router.navigate([route]);
    }

    catagoryDescription(catagory_list:string){
        // console.log(catagory_list);
        if(catagory_list == null || catagory_list == '' || catagory_list == undefined) {
          // return '';
          this.catagory_name = '';
        }
        let catagory = catagory_list.split(',');
        // console.log(catagory);
        let _catagory_name: string = '';
        if(catagory.length >0) {
          catagory.forEach(element => {
            let rs = this.itemCatagory.find(x => x.catagory_id == element);
            console.log(rs);
            if(rs.catagory_name != undefined){
              _catagory_name += rs.catagory_name + ',';
            } 
          });
        }
        // return catagory_name.substring(0, catagory_name.length - 1);
        this.catagory_name = _catagory_name.substring(0, _catagory_name.length - 1);
      }
}
