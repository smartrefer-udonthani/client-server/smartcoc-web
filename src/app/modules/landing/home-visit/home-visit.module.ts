import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { HomeVisitComponent } from 'app/modules/landing/home-visit/home-visit.component';
import { homeVisitRoutes } from 'app/modules/landing/home-visit/home-visit.routing';
import { MatDialogModule} from '@angular/material/dialog';
import { Dialog2q } from './dialog-2q/dialog-2q.component';
import { Dialog8q } from './dialog-8q/dialog-8q.component';
import { Dialog9q } from './dialog-9q/dialog-9q.component';
import {FormGroup, FormControl} from '@angular/forms';
import {Component} from '@angular/core';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';

@NgModule({
    declarations: [
        HomeVisitComponent,
        Dialog2q,
        Dialog8q,
        Dialog9q
    ],
    imports     : [
        RouterModule.forChild(homeVisitRoutes),
        MatDialogModule,
        SharedModule,
        NgxDaterangepickerMd.forRoot()
    ]
})
export class HomeVisitModule
{

}
