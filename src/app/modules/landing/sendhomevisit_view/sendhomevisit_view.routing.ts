import { Route } from '@angular/router';
import {SendhomevisitViewComponent } from 'app/modules/landing/sendhomevisit_view/sendhomevisit_view.component';

export const sendhomevisitViewRoutes: Route[] = [
    {
        path     : '',
        component: SendhomevisitViewComponent
    }
];
