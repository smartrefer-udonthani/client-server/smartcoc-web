export interface DialogData {
    lastVisit: string;
    coc_home_id: string;
    evaluate: any;
    comment?: any;
}

export interface QuestionData {
    question_text: string;
    question_type_id: number;
    evaluate_data?: any;
    evaluate_score?: any;
}

export interface HomeVisitData {
    coc_home_id: number;
    coc_regis_id: number;
    coc_status: string;
    address_lat?: string;
    address_long?: string;
    bmi?: string;
    age?: string;
    body_height?: number;
    body_weight?: string;
    body_temp?: string;
    care_giver_name?: string;
    care_giver_contact?: string;
    d_update?: any;
    diag_text?: string;
    disability_card?: string;
    drugs_use_other?: string;
    dtx?: number;
    durgs_use?: string;
    evaluate_clause?: any;
    extra_detail?: string;
    home_visit_date?: any;
    home_item?: string;
    gait_aid?: string;
    is_active?: string;
    med_use?: string;
    med_use_problem?: string;
    o2sat?: number;
    patient_address_name?: string;
    phone_number?: string;
    plan_code?: string;
    plan_name?: string;
    present_gender?: string;
    problem_and_comment?: string;
    pulse_rate?: number;
    respiratory_rate?: number;
    result?: any[];
    result_code?: string;
    result_name?: string;
    dbp?: number;
    sbp?: number;
    self_use?: string;
    self_use_list?: string;
    summary?: string;
}
export interface PeriodicElementDrug {
    action: any;
    drugdate: string;
    drugname: string;
    druguse: string;
    drugqty: number;
  }
  
  export interface PeriodicElementLab {
    action: any;
    labdate: string;
    labname: string;
    labresult: string;
    labnormal: string;
  }
  
  export interface PeriodicElementXray {
    action: any;
    xraydate: string;
    xrayname: string;
    xrayresult: string;
    comments: string;
  }
  
  export interface PeriodicElementDrugAllergy {
    action: any;
    drugallergydate: string;
    drugallergyname: string;
    drugallergysymptom: string;
    drugallergylevel: string;
    drugallergyfind: string;
  }