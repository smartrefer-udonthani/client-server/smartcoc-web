import { Router, Navigation } from '@angular/router';
import { Component, ViewEncapsulation, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import { DialogData } from '../dialog-data';

@Component({
    selector: 'landing-evaluate-pps',
    templateUrl: './pps.component.html',
    encapsulation: ViewEncapsulation.None
})


export class PpsDialog {
    userFullname: string;
    validateForm: boolean = false;
    lastVisit: string;
    rowsQuestions: any;

    profilePatient: any;   
    score = [
        { point : '100'},
        { point : '90'},
        { point : '80'},
        { point : '70'},
        { point : '60'},
        { point : '50'},
        { point : '40'},
        { point : '30'},
        { point : '20'},
        { point : '10'},
    ];

    evaluate :any;
    title : string;

    constructor(
        private router: Router,
        public matDialogRef: MatDialogRef<PpsDialog>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData,
    ) {
        this.userFullname = localStorage.getItem('userFullname');
        this.profilePatient = JSON.parse(localStorage.getItem('profileData'));
    }

    ngOnInit(): void {
        this.title = this.data.evaluate.question_text;
        this.evaluate = this.data.evaluate;
        this.lastVisit = this.data.lastVisit;
        //console.log(this.evaluate);
    }

    close(): void {
        this.matDialogRef.close();
    }

}
