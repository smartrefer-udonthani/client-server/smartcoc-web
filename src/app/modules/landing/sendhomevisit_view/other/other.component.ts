import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'landing-evaluate-other',
    templateUrl: './other.component.html',
    encapsulation: ViewEncapsulation.None
})
export class OtherDialog{
    sdate: any;
    edate: any;
    validateForm:boolean = false;
    /**
     * Constructor
     */
    constructor() {
    }
}
