export interface Register {
    coc_register_id: number;
    refer_hospcode: string;
    refer_to_hospcode: string;
    refer_level: string;
    patient_status: string;
    title: string;
    first_name: string;
    middle_name?: string;
    last_name?: string;
    gender?: string;
    gender_name?: string;
    cid?: string;
    dob?: string;
    age?: string;
    patient_address_name?: string;
    phone_number?: string;
    patient_right: string;
    discharge_date: string;
    diag_text?: string;
    extra_detail?: string;
    refer_cause?: string;
    coc_home_visit_date?: string;
    coc_home_id?: number;
}