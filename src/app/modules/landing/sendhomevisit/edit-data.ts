export interface PatientInfo {
    cid: string;
    title: string;
    first_name: string;
    last_name: string;
    gender_name: string;
    patient_address_name: string;
    phone_number: string;
    villageCode: string;
    house_no: string;

}