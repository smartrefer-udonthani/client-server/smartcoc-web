import { Component, ViewEncapsulation } from '@angular/core';
import { RegisterService } from '../../../services/register.service';
import { ThaiaddressService } from '../../../services/thaiaddress.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApexOptions } from 'ng-apexcharts';
import { round } from 'lodash';
import { Router } from '@angular/router';

@Component({
    selector: 'landing-home',
    templateUrl: './home.component.html',
    encapsulation: ViewEncapsulation.None
})

export class LandingHomeComponent {
    data_send: any;
    data_result: any;
    total_send: number = 0;
    total_home_visit: number = 0;
    total_visit: number = 0;
    total_home: number = 0;
    total_continue: number = 0;
    total_better: number = 0;
    total_worse: number = 0;
    total_death: number = 0;
    total_refer: number = 0;
    total_stable: number = 0;
    rate_visit : number = 0;
    rate_home_visit : number = 0;

    chartData: ApexOptions = {};

    hosp_send:any = [
        { "hospcode": "10669", "hospname": "โรงพยาบาลสรรพสิทธิประสงค์" ,"total": 78},
        { "hospcode": "10954", "hospname": "โรงพยาบาลวารินชำราบ" ,"total": 64},
        { "hospcode": "21984", "hospname": "โรงพยาบาล ๕๐ พรรษาฯ" ,"total": 53},
        { "hospcode": "11443", "hospname": "โรงพยาบาลสมเด็จพระยุพราชเดชอุดม" ,"total": 45},
        { "hospcode": "10951", "hospname": "โรงพยาบาลพิบูลมังสาหาร" ,"total": 32},
        { "hospcode": "10952", "hospname": "โรงพยาบาลตระการ" ,"total": 21},
    ];

    hcode: any = localStorage.getItem('hcode');
    info: any = {
        "hcode": this.hcode
    }
    title: string;

    is_reload_data: boolean = false;

    /**
     * Constructor
     */
    constructor(
        private registerService: RegisterService, 
        private thaiaddressService: ThaiaddressService,
        private spinner: NgxSpinnerService, 
        private router: Router
    ) {
        let username = sessionStorage.getItem('username');
        if(!username){
            this.router.navigate(['/sign-in']);
        }
        let hospital_data = localStorage.getItem('list_all_hospital');
        if(hospital_data){
            let data = JSON.parse(hospital_data);
            if(data.length == 0){
                this.is_reload_data = true;
            }
        } else {
            this.is_reload_data = true;
        }
    }

    ngOnInit() {
        this.spinner.show();
        this.get_total_send();
        this.get_total_visit();
        this._prepare_chart_data();
        if(this.is_reload_data){
            this.getAllHospital();
        }
    //    this.get_result();
    //    this.select_get_result();
    //    this.get_plan();
        this.spinner.hide();
    }            

    ngAfterViewInit() {
       // this.title = 'ระบบสารสนเทศสุขภาพ';
    }

    async get_total_send() {
        try {
            let rs: any = await this.registerService.get_total_send(this.info);
            this.data_send = rs[0];
            // console.log(this.data_send);
            this.title = this.data_send.hosname;
            this.total_send = this.data_send.total;
            this.total_home_visit = this.data_send.total_visit;
            this.rate_visit = round(this.total_home_visit / this.total_send * 100,2);
            // console.log(this.data_send);    
        } catch (error) {
            console.log(error.error);
        }
    }
    async get_total_visit() {
        try {
            let rs: any = await this.registerService.get_total_visit(this.info);
            this.data_result = rs[0];
            // console.log(this.data_result);
            this.total_visit = this.data_result.total;
            this.total_home = this.data_result.home;
            this.total_continue = this.data_result.visit;
            this.total_better = this.data_result.better;
            this.total_worse = this.data_result.worst;
            this.total_death = this.data_result.stop_coc;
            this.total_refer = this.data_result.refer;
            this.total_stable = this.data_result.stable;
            this.rate_home_visit = round(this.total_home / this.total_visit * 100,2);
        } catch (error) {
            console.log(error.error);
        }
    }

    async get_result() {
        try {
            let rs: any = await this.registerService.get_result(this.info);
            this.data_result = rs[0];
            // console.log(this.data_result);
        } catch (error) {
            console.log(error);
        }
    }

    async select_get_result() {
        try {
            let rs: any = await this.registerService.get_result_visit(this.info);
            this.data_result = rs[0];
            // console.log(this.data_result);
        } catch (error) {
            console.log(error);
        }
    }

    async get_plan() {
        try {
            let rs: any = await this.registerService.get_plan(this.info);
            this.data_result = rs[0];
            // console.log(this.data_result);
        } catch (error) {
            console.log(error);
        }
    }

    private _prepare_chart_data():void {
        this.chartData = {
            chart      : {
                fontFamily: 'inherit',
                foreColor : 'inherit',
                height    : '100%',
                type      : 'line',
                toolbar   : {
                    show: false
                },
                zoom      : {
                    enabled: false
                }
            },
            colors     : ['#537FE7', '#94A3B8'],
            dataLabels : {
                enabled        : true,
                enabledOnSeries: [0],
                background     : {
                    borderWidth: 0
                }
            },
            grid       : {
                borderColor: 'var(--fuse-border)'
            },
            labels     : this.hosp_send.map(item => item.hospname),
            legend     : {
                show: false
            },
            plotOptions: {
                bar: {
                    columnWidth: '50%'
                }
            },
            series     : [
                {
                    name : 'Total',
                    type : 'column',
                    data : this.hosp_send.map(item => item.total),
                }
            ],
            states     : {
                hover: {
                    filter: {
                        type : 'darken',
                        value: 0.75
                    }
                }
            },
            stroke     : {
                width: [3, 0]
            },
            tooltip    : {
                followCursor: true,
                theme       : 'dark'
            },
            xaxis      : {
                axisBorder: {
                    show: false
                },
                axisTicks : {
                    color: 'var(--fuse-border)'
                },
                labels    : {
                    style: {
                        colors: 'var(--fuse-text-secondary)'
                    }
                },
                tooltip   : {
                    enabled: false
                }
            },
            yaxis      : {
                labels: {
                    offsetX: -16,
                    style  : {
                        colors: 'var(--fuse-text-secondary)'
                    }
                }
            }

        };
        // console.log(this.chartData);
    }

    async getAllHospital() {
        await localStorage.removeItem('list_all_hospital');

        await this.thaiaddressService.select_all().then((res: any) => { 
            localStorage.setItem('list_all_hospital', JSON.stringify(res));
            this.is_reload_data = true;
        });
    }

    async getAllDiagnosis() {
        await localStorage.removeItem('list_icd10');

        await this.thaiaddressService.getIcd10().then((res: any) => { 
            localStorage.setItem('list_icd10', JSON.stringify(res));
            this.is_reload_data = true;
        });
    }
}
