    import { Inject, Injectable } from '@angular/core';
    import { HttpClient, HttpHeaders } from '@angular/common/http';
    import { catchError, Observable, of, switchMap, throwError } from 'rxjs';

    @Injectable({
      providedIn: 'root'
    })
    export class CvdRiskService {
    
      accessToken: any;
      httpOptions: any;
    
      constructor(@Inject('API_URL') private apiUrl: string, private httpClient: HttpClient) {
        this.accessToken = sessionStorage.getItem('accessToken');
        this.httpOptions = {
          headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.accessToken
          })
        };
      }

    async list(){
          const _url = `${this.apiUrl}/coc_refer_level/select`;
         return await this.httpClient.get(_url).toPromise();
    }
    
  
    
    //   async save(info:object) {
    //     const _url = `${this.apiUrl}/patient`;
    //     return this.httpClient.post(_url,info,this.httpOptions).toPromise();
    //   }    
    
    //   async update(id:any,info:object,) {
    //     const _url = `${this.apiUrl}/patient/${id}`;
    //     return this.httpClient.put(_url,info,this.httpOptions).toPromise();
    //   }    
    
    //   async delete(id:any) {
    //     const _url = `${this.apiUrl}/patient/${id}`;
    //     return this.httpClient.delete(_url,this.httpOptions).toPromise();
    //   } 
    
    
    }
